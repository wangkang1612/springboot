package cn.itcast.demo;

import cn.itcast.demo.pojo.Heima62;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.junit.Test;

import java.util.List;

public class SolrIndexSuperQueryTest {



    //范围查询包含边界，不要过度设计
    @Test
    public void testRangeQuery() throws SolrServerException {
        SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/core2");


        //solr的查询条件
        SolrQuery solrQuery = new SolrQuery("price:[* TO 199999]");

        //发起web请求查询solr中对应的数据，并获取响应
        QueryResponse queryResponse = solrServer.query(solrQuery);


        //把查询的返回结果解析到JavaBean中
        List<Heima62> heima62s = queryResponse.getBeans(Heima62.class);


        heima62s.forEach(heima62 -> {
            String id = heima62.getId();

            String title = heima62.getTitle();

            Long price = heima62.getPrice();

            System.out.println("id:"+id+" title:"+title+" price:"+price);
        });

    }


    //组合查询，AND  OR   NOT
    @Test
    public void testBooleanQuery() throws SolrServerException {
        SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/core2");


        //solr的查询条件
        SolrQuery solrQuery = new SolrQuery("title:手机 AND  price:[300000 TO 400000] NOT title:华为");

        //发起web请求查询solr中对应的数据，并获取响应
        QueryResponse queryResponse = solrServer.query(solrQuery);


        //把查询的返回结果解析到JavaBean中
        List<Heima62> heima62s = queryResponse.getBeans(Heima62.class);


        heima62s.forEach(heima62 -> {
            String id = heima62.getId();

            String title = heima62.getTitle();

            Long price = heima62.getPrice();

            System.out.println("id:"+id+" title:"+title+" price:"+price);
        });

    }



    //模糊查询的开启需要加约等号~
    @Test
    public void testFuzzyQuery() throws SolrServerException {
        SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/core2");


        //solr的查询条件
        SolrQuery solrQuery = new SolrQuery("title:viva~1");

        //发起web请求查询solr中对应的数据，并获取响应
        QueryResponse queryResponse = solrServer.query(solrQuery);


        //把查询的返回结果解析到JavaBean中
        List<Heima62> heima62s = queryResponse.getBeans(Heima62.class);


        heima62s.forEach(heima62 -> {
            String id = heima62.getId();

            String title = heima62.getTitle();

            Long price = heima62.getPrice();

            System.out.println("id:"+id+" title:"+title+" price:"+price);
        });

    }
}
