package cn.itcast.demo.cloud;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Test;

import java.io.IOException;

public class SolrcloudCreateIndexTest {


    @Test
    public void testCreateIndex() throws IOException, SolrServerException {
        CloudSolrServer solrServer = new CloudSolrServer("192.168.206.101:2181,192.168.206.102:2181,192.168.206.103:2181");


        solrServer.setDefaultCollection("myCollection1");


        SolrInputDocument document = new SolrInputDocument();
        document.addField("id","12345");
        document.addField("title","春天来了，出去踏青");


        solrServer.add(document);

        solrServer.commit();
    }
}
