package cn.itcast.demo;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.junit.Test;

import java.io.IOException;

public class SolrIndexUpdate {


    @Test
    public void testUpdate() throws IOException, SolrServerException {
        SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/core2");

        //solrServer.deleteById("10010");

        solrServer.deleteByQuery("title:手机");

        solrServer.commit();
    }
}
