package cn.itcast.demo.pojo;

import org.apache.solr.client.solrj.beans.Field;

public class Heima62 {

    @Field
    private String id;
    @Field
    private String title;
    @Field
    private Long price;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
