package cn.itcast.demo;

import cn.itcast.demo.pojo.Heima62;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.junit.Test;

import java.util.List;

public class SolrIndexQueryTest {


    @Test
    public void testQueryIndex() throws SolrServerException {
        SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/core2");


        //solr的查询条件
        SolrQuery solrQuery = new SolrQuery("title:手机");

        //发起web请求查询solr中对应的数据，并获取响应
        QueryResponse queryResponse = solrServer.query(solrQuery);

        //获取查询到的所有的文档的数据集合
        SolrDocumentList results = queryResponse.getResults();

        results.forEach(result->{//jdk8

            String id = (String) result.get("id");
            String title = (String) result.get("title");
            Long price = (Long) result.get("price");

            System.out.println("id:"+id+" title:"+title+" price:"+price);
        });
    }


    @Test
    public void testQueryIndexWithBean() throws SolrServerException {
        SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/core2");


        //solr的查询条件
        SolrQuery solrQuery = new SolrQuery("title:手机");

        //发起web请求查询solr中对应的数据，并获取响应
        QueryResponse queryResponse = solrServer.query(solrQuery);


        //把查询的返回结果解析到JavaBean中
        List<Heima62> heima62s = queryResponse.getBeans(Heima62.class);


        heima62s.forEach(heima62 -> {
            String id = heima62.getId();

            String title = heima62.getTitle();

            Long price = heima62.getPrice();

            System.out.println("id:"+id+" title:"+title+" price:"+price);
        });

    }
}
