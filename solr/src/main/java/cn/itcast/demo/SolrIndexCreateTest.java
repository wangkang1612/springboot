package cn.itcast.demo;

import cn.itcast.demo.pojo.Heima62;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Test;

import java.io.IOException;

public class SolrIndexCreateTest {


    @Test
    public void testCreateIndex() throws IOException, SolrServerException {

        SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/core2");

        SolrInputDocument document = new SolrInputDocument();

        document.addField("id","10086");
        document.addField("title","黑马手机");
        document.addField("price",12345);

        solrServer.add(document);

        solrServer.commit();
    }

    @Test
    public void testUpdateIndex() throws IOException, SolrServerException {

        SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/core2");

        SolrInputDocument document = new SolrInputDocument();

        document.addField("id","10086");
        document.addField("title","黑马手机,手机中的黑马");
        document.addField("price",54321);

        solrServer.add(document);

        solrServer.commit();
    }


    @Test
    public void testCreateIndexWithBean() throws IOException, SolrServerException {

        SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/core2");

        Heima62 heima62 = new Heima62();
        heima62.setId("10010");
        heima62.setTitle("吊炸天");
        heima62.setPrice(1800000L);

        solrServer.addBean(heima62);

        solrServer.commit();
    }
}
