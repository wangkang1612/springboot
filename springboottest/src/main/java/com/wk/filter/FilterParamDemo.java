package com.wk.filter;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class FilterParamDemo {
    @Value("${test.name}")
    public String name;
    //动态读取yml里的配置,做一些拦截控制
    private final FilterParam filterParam;

    //先注入 过滤对象
    public FilterParamDemo(FilterParam filterParam) {
        this.filterParam = filterParam;
    }

    @GetMapping("/bb")
    public String hello1() {
        System.out.println("name = " + name);
        return "hello" + name;
    }

    @GetMapping("/aa")
    public Boolean hello() {
        System.out.println("name = " + filterParam.getSss());
        Map<String, List<String>> filter = filterParam.getFilterParam();
        System.out.println("filter = " + filter);
        List<String> list = filter.get("demo1");
        String str = "1";
        Boolean flag = false;
        if (CollectionUtils.isNotEmpty(list)) {
            for (String s : list) {
                if (s.contains(str)) {
                    flag = true;
                }
            }

        }
        for (String s : list) {
            System.out.println("s = " + s);
        }
        //避免空指针

        String s = Optional.ofNullable(name).orElse("代替空指针");
        System.out.println("s = " + s);
        return flag;
    }


}
