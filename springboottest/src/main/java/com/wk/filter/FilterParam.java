package com.wk.filter;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
@Data
@Component
@Configuration
@ConfigurationProperties(prefix = "filter")
public class FilterParam {
    private String sss;
    private Map<String, List<String>> filterParam;



}
