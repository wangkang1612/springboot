package com.wk.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User  {

    // 对应数据库中的主键 (uuid、自增id、雪花算法、redis、zookeeper！)
    //@TableId(type = IdType.AUTO)
    public Long id;
    public String name;
    public Integer age;
    public String email;
    public String adress;


}
