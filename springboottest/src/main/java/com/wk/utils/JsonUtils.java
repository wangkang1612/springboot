package com.wk.utils;


import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.wk.pojo.User;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class JsonUtils {

    // 定义jackson对象
    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * 将对象转换成json字符串�??
     * <p>Title: pojoToJson</p>
     * <p>Description: </p>
     * @param data
     * @return
     */
    public static String objectToJson(Object data) {
        try {
            String string = MAPPER.writeValueAsString(data);
            return string;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将json结果集转化为对象
     *
     * @param jsonData json数据
     * @param  beanType 对象中的object类型
     * @return
     */
    public static <T> T jsonToPojo(String jsonData, Class<T> beanType) {
        try {
            T t = MAPPER.readValue(jsonData, beanType);
            return t;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将json数据转换成pojo对象list
     * <p>Title: jsonToList</p>
     * <p>Description: </p>
     * @param jsonData
     * @param beanType
     * @return
     */
    public static <T>List<T> jsonToList(String jsonData, Class<T> beanType) {
        JavaType javaType = MAPPER.getTypeFactory().constructParametricType(List.class, beanType);
        try {
            List<T> list = MAPPER.readValue(jsonData, javaType);
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static <T> T getJson(String jsonString, Class<T> cls) {
        T t = null;
        try {
            t = JSON.parseObject(jsonString, cls);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return t;
    }

    public static <T> List<T> getArrayJson(String jsonString, Class<T> cls) {
        List<T> list = new ArrayList<T>();
        try {
            list = JSON.parseArray(jsonString, cls);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return list;
    }

    public static void main(String[] args) throws Exception {
        /*testObj();*/
        /*testList();*/
        testMap();
    }
    /**
     * 1.对象转json格式的字符串
     * 2.对象转字节数组
     * 3.json字符串转为对象
     * 4.byte数组转为对象
     * @throws Exception
     */
    public static void testObj() throws Exception {
        //创建转换对象 使用默认的构造方法，使用的是JsonParsers和JsonGenerators映射
        ObjectMapper mapper=new ObjectMapper();
        // 转换为格式化的json
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        // 如果json中有新增的字段并且是实体类类中不存在的，不报错
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        //创建学生对象
        User stu=new User();
        //1.对象转json格式的字符串
        String stuToString = mapper.writeValueAsString(stu);
        //System.out.println("对象转为字符串：" + stuToString);
	    /*
	     * 输出结果：
	     * 对象转为字符串：{
					  "name" : "ghl",
					  "age" : 12,
					  "password" : "123",
					  "email" : "19845252@qq.com"
				}
	     */


        //2.对象转字节数组
        byte[] byteArr = mapper.writeValueAsBytes(stu);
        System.out.println("对象转为byte数组：" + byteArr);
        /*
         * 对象转为byte数组：[B@6aaa5eb0
         */


        //3.json字符串转为对象
        User student = mapper.readValue(stuToString, User.class);
        System.out.println("json字符串转为对象：" + student);
        //json字符串转为对象：Student [name=ghl, age=12, password=123, email=19845252@qq.com]


        //4.byte数组转为对象
        User student2 = mapper.readValue(byteArr, User.class);
        System.out.println("byte数组转为对象：" + student2);
        //输出结果：byte数组转为对象：Student [name=ghl, age=12, password=123, email=19845252@qq.com]


        mapper.writeValue(new File("D:/test.txt"), stu); // 写到文件中
    }

    /**list集合与json字符的转换
     * 1.集合转为字符串
     * 2.字符串转集合
     * @throws Exception
     *
     */
    public static void testList() throws Exception {
        //创建转换对象 使用默认的构造方法，使用的是JsonParsers和JsonGenerators映射
        ObjectMapper mapper=new ObjectMapper();
        // 转换为格式化的json
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        // 如果json中有新增的字段并且是实体类类中不存在的，不报错
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        List<User> studnetList = new ArrayList<User>();
        studnetList.add(new User());
        //1.集合转为字符串
        String jsonStr = mapper.writeValueAsString(studnetList);
        System.out.println("集合转为字符串：" + jsonStr);
	     /*輸出結果
	      * 集合转为字符串：[ {
					  "name" : "zs",
					  "age" : 12,
					  "password" : "121",
					  "email" : "1584578878@qq.com"
					}, {
					  "name" : "lisi",
					  "age" : 13,
					  "password" : "122",
					  "email" : "1584578878@qq.com"
					}, {
					  "name" : "wangwu",
					  "age" : 14,
					  "password" : "123",
					  "email" : "1584578878@qq.com"
					}, {
					  "name" : "zhangliu",
					  "age" : 15,
					  "password" : "124",
					  "email" : "1584578878@qq.com"
					} ]
	      *
	      */


        //2.字符串转集合
        List<User> userListDes = mapper.readValue(jsonStr, List.class);
        System.out.println("字符串转集合：" + userListDes);
        /*輸出結果
         * 字符串转集合：[{name=zs, age=12, password=121, email=1584578878@qq.com},
         * {name=lisi, age=13, password=122, email=1584578878@qq.com},
         * {name=wangwu, age=14, password=123, email=1584578878@qq.com},
         * {name=zhangliu, age=15, password=124, email=1584578878@qq.com}]
         */
    }

    /**Map与字符串转换
     * 1.Map转为字符串
     * 2.字符串转Map
     * @throws Exception
     */
    public static void testMap() throws Exception {
        //创建转换对象 使用默认的构造方法，使用的是JsonParsers和JsonGenerators映射
        ObjectMapper mapper=new ObjectMapper();
        // 转换为格式化的json
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        // 如果json中有新增的字段并且是实体类类中不存在的，不报错
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Map<String, Object> testMap = new HashMap<String, Object>();
        testMap.put("name", "ghl");
        testMap.put("age", 18);
        //1.Map转为字符串
        String jsonStr = mapper.writeValueAsString(testMap);
        System.out.println("Map转为字符串：" + jsonStr);
            /*
             * Map转为字符串：{
						  "name" : "ghl",
						  "age" : 18
						}
             */
        //2.字符串转Map
        Map<String, Object> testMapDes = mapper.readValue(jsonStr, Map.class);
        System.out.println("字符串转Map：" + testMapDes);
        /*
         * 字符串转Map：{name=ghl, age=18}
         */
    }



}

