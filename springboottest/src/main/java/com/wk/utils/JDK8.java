package com.wk.utils;

import com.wk.filter.FilterParam;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JDK8 {
    //https://blog.csdn.net/weixin_45840947/article/details/122656557
    public static FilterParam filterParam;
    public static void main(String[] args) {
        isNull();
        isNull1();
        list();
    }
    public static void list() {
        String[] arr={"a","ab","ac"};
        Stream<String> arr1 = Stream.of(arr);
        Stream<String> a = arr1.filter(e -> e.contains("a"));
        Map<String, String> map = a.collect(Collectors.toMap(e -> e.toUpperCase(), e -> e.toLowerCase()));
        System.out.println("map = " + map);
    }
    public static void isNull1() {
        Optional<String> aa = Optional.ofNullable("aa");
        Optional<String> bb = Optional.ofNullable(null);
        System.out.println("a= " + aa.isPresent()+",b= "+bb.isPresent());
    }
    public static void isNull(){
        //避免空指针
        FilterParam filterParam = Optional.ofNullable(JDK8.filterParam).orElse(new FilterParam());
        System.out.println("s = " + filterParam);

        String str=null;
        String s = Optional.ofNullable(str).orElse("空指针");
        System.out.println("s = " + s);
    }
}
