package com.wk.utils.csv;


import com.wk.utils.csv.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//https://blog.csdn.net/yjltx1234csdn/article/details/121235803
public class DataUtil {
    /**
     * 获取用户的数据
     * @date 2021/11/8 13:51
     * @author zk_yjl
     * @param name
     * @return java.util.List<top.yueshushu.learn.pojo.User>
     */
    public static List<User> createDataList(String name) {
        List<User> result=new ArrayList<>();
        for(int i=1;i<=10;i++){
            User user=new User();
            user.setId(i);
            user.setName(name+"_"+i);
            user.setSex(i%2==0?"女":"男");
            user.setAge(20+i);
            user.setDescription("我是第"+i+"个，我的名字是:"+user.getName());
            result.add(user);
        }
        return result;
    }

    public static List<Map<String,String>> createDataMapList() {
        List<Map<String,String>> result=new ArrayList<>();
        for(int i=1;i<=10;i++){
            Map<String,String> map=new HashMap<>();
            map.put(i+"key","我是第"+i+"个,我的名字逗号");
            result.add(map);
        }
        return result;
    }

}
