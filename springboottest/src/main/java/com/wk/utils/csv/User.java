package com.wk.utils.csv;

import cn.hutool.core.annotation.Alias;
import lombok.Data;

@Data
public class User {
    @Alias("编号")
    private Integer id;
    @Alias("姓名")
    private String name;
 //   @Alias("性别")
    private String sex;
  //  @Alias("年龄")
    private Integer age;
    @Alias("描述信息")
    private String description;
}

