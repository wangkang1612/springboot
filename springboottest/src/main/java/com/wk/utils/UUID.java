package com.wk.utils;

public class UUID {

    public static String getUUID() {
        return java.util.UUID.randomUUID().toString().replaceAll("-", "");
    }
    public static String getUUID1() {
        return java.util.UUID.randomUUID().toString();
    }

    public static void main(String[] args) {
        System.out.println(getUUID());
        System.out.println(getUUID1());
    }

}