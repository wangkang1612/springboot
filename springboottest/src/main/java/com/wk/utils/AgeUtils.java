package com.wk.utils;

import java.time.LocalDate;
import java.time.Period;

public class AgeUtils {

	public static void main(String[] args) {
		calcAgeForIdcard("350104198306020056");
		calcAgeForBirthday("1996-09-08");
        calcAgeForBirthday("1999-09-22");
	}

	// 根据身份证号计算年龄
	private static void calcAgeForIdcard(String date) {
		int year = Integer.parseInt(date.substring(6).substring(0, 4));   // 得到出身年份
        int momth = Integer.parseInt(date.substring(10).substring(0, 2)); // 得到出身月份
        int day = Integer.parseInt(date.substring(12).substring(0, 2));   // 得到出身天数
		calcAge(year, momth, day);
	}
	
	// 根据输入的出身年月日计算年龄
    private static void calcAgeForBirthday(String date) {
        int year = Integer.parseInt(date.substring(0, 4));   // 得到出身年份
        int month = Integer.parseInt(date.substring(5).substring(0, 2)); // 得到出身月份
        int day = Integer.parseInt(date.substring(8).substring(0, 2));   // 得到出身天数
        calcAge(year, month, day);
    }

	private static void calcAge(int year, int month, int day) {
		LocalDate birthday = LocalDate.of(year, month, day);
        System.out.println("Birthday: " + birthday);
        
        LocalDate today = LocalDate.now();
        System.out.println("Today: " + today);
        
        Period p = Period.between(birthday, today);
        System.out.printf("当前年龄: %d 年 %d 月 %d 日", p.getYears(), p.getMonths(), p.getDays());
        System.out.println("\n");
	}
}
