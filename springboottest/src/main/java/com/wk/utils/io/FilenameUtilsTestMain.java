package com.wk.utils.io;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilenameUtilsTestMain {
    public static void main(String[] args) throws IOException  {
        //FilenameUtils类中常用方法
        File file = new File("D:\\javaTool\\test.txt");
        // 返回文件后缀名
        System.out.println(FilenameUtils.getExtension(file.toString()));
        // 返回文件名，不包含后缀名
        System.out.println(FilenameUtils.getBaseName(file.toString()));
        // 返回文件名，包含后缀名
        System.out.println(FilenameUtils.getName(file.toString()));
        // 获取文件的路径（不带文件名）
        System.out.println(FilenameUtils.getFullPath(file.toString()));
        // 路径分隔符改成unix系统格式的，即/
        System.out.println(FilenameUtils.separatorsToUnix(file.toString()));
        // 检查文件后缀名是不是传入参数(List<String>)中的一个
        System.out.println(FilenameUtils.isExtension("txt", new ArrayList<>()));
        fileReader();
        fileWriter();
    }

    public static void fileReader() throws IOException {
        // 定义一个文件
        File f = new File("D:\\A_code\\mybatis-plus\\kuang_livenote-master\\【遇见狂神说】MyBatisPlus视频笔记\\代码\\mybatis_plus\\src\\main\\resources\\test.txt");
        String encoding = "UTF-8";

        // 01.把文件的所有内容放到String里,第二个参数是编码可选的..
        System.out.println("-------------把文件的所有内容读到String中---------");
        String readFileToString = FileUtils.readFileToString(f, encoding);
        System.out.println(readFileToString);

        // 02.把文件的所有内容按行放到List中.第二个参数是编码,可选
        System.out.println("-------------把文件的所有内容按行读到List中---------");
        List<String> readLines = FileUtils.readLines(f, encoding);
        readLines.forEach(System.out::println);

        // 03.读成字节数组(一般用于二进制文件)
        System.out.println("-------------把文件的所有内容读成一个byteArray---------");
        byte[] readFileToByteArray = FileUtils.readFileToByteArray(f);
        System.out.println(new String(readFileToByteArray, encoding));

    }

    public static void fileWriter() throws IOException {
        File f = new File("D:\\javaTool\\test.txt");
        File f1 = new File("D:\\A_code\\mybatis-plus\\kuang_livenote-master\\【遇见狂神说】MyBatisPlus视频笔记\\代码\\mybatis_plus\\src\\main\\resources\\test.txt");

        //定义一个文件
        Charset c = StandardCharsets.UTF_8;
        String s = "你好,世界\n hello world";
        List <String> lines = Stream.of("你好,世界", "hello, world").collect(Collectors.toList());
         s = FileUtils.readFileToString(f1, c);

        //01.把内容写进文件
       // FileUtils.write(f, s);
        //02.把内容写进文件,用UTF-8编码
        FileUtils.write(f, s, c);
        //03.把内容追加进文件
        FileUtils.write(f, s, true);
        //04.把一个List写进文件,List的每一个元素是一行
        FileUtils.writeLines(f, lines);
        FileUtils.writeLines(f, lines, true);
        FileUtils.writeLines(f, "UTF-8", lines, true);
        //05.写入二进制数据
        FileUtils.writeByteArrayToFile(f, s.getBytes());
        //06.还有writeStringtoFile
        //FileUtils.writeStringToFile(f, fileContent, c);
    }
}