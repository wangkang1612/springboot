package com.wk.utils.io;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

public class OtherFileOptionTestMain {
    public static void main(String[] args) throws Exception {
        String src1="D:\\A_code\\mybatis-plus\\kuang_livenote-master\\【遇见狂神说】MyBatisPlus视频笔记\\代码\\mybatis_plus\\src\\main\\resources\\test.txt";
        String desc1="D:\\A_code\\mybatis-plus\\kuang_livenote-master\\【遇见狂神说】MyBatisPlus视频笔记\\代码\\mybatis_plus\\src\\main\\resources\\test1.txt";
        String gif1="D:\\A_code\\mybatis-plus\\kuang_livenote-master\\【遇见狂神说】MyBatisPlus视频笔记\\代码\\mybatis_plus\\src\\main\\resources\\test1.txt";
        File src = new File(src1);
        File dest = new File(desc1);
        // 01文件copy操作
        FileUtils.copyFile(src, dest);
        //FileUtils.copyFileToDirectory(src, new File(desc1));
        // 02下载文件到本地
        InputStream in = new URL("http://www.baidu.com/img/baidu_logo.gif").openStream();
        byte[] gif = IOUtils.toByteArray(in);
        IOUtils.write(gif, new FileOutputStream(new File(gif1)));
        FileUtils.writeByteArrayToFile(new File(gif1), gif);
        // 03删除文件
       // FileUtils.forceDelete(new File(desc1));
        // 04保存网络文件到本地文件
        FileUtils.copyURLToFile(new URL("http://www.163.com"), new File(desc1));
        // 05清空目录下的文件
        //FileUtils.cleanDirectory(dest);
        // 06删除目录和目录下的文件
        //FileUtils.deleteDirectory(dest);
        //FileUtils.deleteQuietly(dest);
        // 07目录操作（不存在,新建，存在,修改文件修改时间）
        //FileUtils.touch(new File("testFile.txt"));
        // 08相同的内容写入不同的文本
       // File test1 = new File("split1.txt");
        //File test2 = new File("split2.txt");
        //TeeOutputStream teeOutputStream = new TeeOutputStream(new FileOutputStream(test1), new FileOutputStream(test2));
       // teeOutputStream.write("One Two Three, Test".getBytes());
       // teeOutputStream.flush();
        // 09目录大小
        long sizeOf = FileUtils.sizeOf(new File(src1));
        //long sizeOfDirectory = FileUtils.sizeOfDirectory(new File(src1));
       // System.out.println(sizeOf == sizeOfDirectory);
        // 10文件流copy到文件中.这个在Spring MVC,文件上传时常用举个MVC的例子
     //   CommonsMultipartFile mf = null; //此处是Spring MVC上传时的文件接收对象
      //  FileUtils.copyInputStreamToFile(mf.getInputStream(), new File(""));
    }
}