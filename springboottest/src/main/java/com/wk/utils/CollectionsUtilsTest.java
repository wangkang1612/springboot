package com.wk.utils;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CollectionsUtilsTest {
    public static void main(String[] args) throws ParseException {
       Map map=new HashMap();
        MapUtils.getInteger(map,"q");//null
        MapUtils.isEmpty(map);//true
        MapUtils.isNotEmpty(map);//false

        List a=new ArrayList<>();
        a.add(1);
        a.add(2);
        a.add("c");
        List b=new ArrayList<>();
        b.add(3);
        b.add(4);
        b.add("c");
        CollectionUtils.intersection(a,b);//c   交集
        CollectionUtils.union(a,b);//1,2,3,4,c   并集
        CollectionUtils.subtract(a,b);//1,2,   差集



    }
    
    public static String getStr(Long num){
        StringBuilder sb=new StringBuilder("");
        String format = String.format("%04d", num);
        sb.append("A").append(format).append("Z");
        return sb.toString();
    }
}
