package com.wk.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.wk.pojo.User;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class JsonUtilsTest {

    private static final ObjectMapper mapper;

    public static ObjectMapper getObjectMapper() {
        return mapper;
    }

    static {
        //创建ObjectMapper对象
        mapper = new ObjectMapper();

        //configure方法 配置一些需要的参数
        // 转换为格式化的json 显示出来的格式美化
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        //序列化的时候序列对象的那些属性
        //JsonInclude.Include.NON_DEFAULT 属性为默认值不序列化
        //JsonInclude.Include.ALWAYS      所有属性
        //JsonInclude.Include.NON_EMPTY   属性为 空（“”） 或者为 NULL 都不序列化
        //JsonInclude.Include.NON_NULL    属性为NULL 不序列化
        mapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);


        //反序列化时,遇到未知属性会不会报错
        //true - 遇到没有的属性就报错 false - 没有的属性不会管，不会报错
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        //如果是空对象的时候,不抛异常
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        // 忽略 transient 修饰的属性
        mapper.configure(MapperFeature.PROPAGATE_TRANSIENT_MARKER, true);

        //修改序列化后日期格式
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

        //处理不同的时区偏移格式
       // mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
      //  mapper.registerModule(new JavaTimeModule());

    }

    public static void main(String[] args) throws IOException {
        jsonToString();
    }

    public static String jsonToString() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        //a.json字符串转对象
        String jsonString = "{\"name\":\"Hyl\", \"age\":20}";
//将字符串转换为对象
        User user = mapper.readValue(jsonString, User.class);
        System.out.println(user);
//将对象转换为json字符串
        jsonString = mapper.writeValueAsString(user);
        System.out.println(jsonString);
       /* 结果：
        Student [ name: Hyl, age: 20 ]
        {
            "name" : "Hyl",
                "age" : 20
        }*/
        //b. 数组和对象之间转换
        //对象转为byte数组
        byte[] byteArr = mapper.writeValueAsBytes(user);
        System.out.println(byteArr);
//byte数组转为对象
        User user1= mapper.readValue(byteArr, User.class);
        System.out.println(user1);

/*        结果：
[B@3327bd23

        Student [ name: Hyl, age: 20 ]*/

        //c. 集合和json字符串之间转换

        List<User> studentList= new ArrayList<>();
//        studentList.add(new User("hyl1" ,20 ));
//        studentList.add(new User("hyl2" ,21 ));
//        studentList.add(new User("hyl3" ,22 ));
//        studentList.add(new User("hyl4" ,23 ));

        String jsonStr = mapper.writeValueAsString(studentList);
        System.out.println(jsonStr);

        List<User> studentList2 = mapper.readValue(jsonStr, List.class);
        System.out.println("字符串转集合：" + studentList2 );
/*
[{"id":null,"name":"hyl1","age":20,"email":null,"version":null,"deleted":null,"createTime":null,"updateTime":null},{"id":null,"name":"hyl2","age":21,"email":null,"version":null,"deleted":null,"createTime":null,"updateTime":null},{"id":null,"name":"hyl3","age":22,"email":null,"version":null,"deleted":null,"createTime":null,"updateTime":null},{"id":null,"name":"hyl4","age":23,"email":null,"version":null,"deleted":null,"createTime":null,"updateTime":null}]
        字符串转集合：[{id=null, name=hyl1, age=20, email=null, version=null, deleted=null, createTime=null, updateTime=null}, {id=null, name=hyl2, age=21, email=null, version=null, deleted=null, createTime=null, updateTime=null}, {id=null, name=hyl3, age=22, email=null, version=null, deleted=null, createTime=null, updateTime=null}, {id=null, name=hyl4, age=23, email=null, version=null, deleted=null, createTime=null, updateTime=null}]
*/

//d. map和json字符串之间转换
        Map<String, Object> testMap = new HashMap<>();
        testMap.put("name", "22");
        testMap.put("age", 20);
        testMap.put("date", new Date());
        //testMap.put("student", new User("hyl", 20));


        String jsonStr1 = mapper.writeValueAsString(testMap);
        System.out.println(jsonStr1);
        Map<String, Object> testMapDes = mapper.readValue(jsonStr1, Map.class);
        System.out.println(testMapDes);
/*
        结果：
        {
            "date" : 1525164212803,
                "name" : "22",
                "student" : {
            "name" : "hyl",
                    "age" : 20,
                    "sendTime" : 1525164212803,
                    "intList" : null
        },
            "age" : 20
        }
        {date=1525164212803, name=22, student={name=hyl, age=20, sendTime=1525164212803, intList=null}, age=20}
*/

//e. 日期转json字符串
        // 修改时间格式
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
//        User student = new User ("hyl",21);
//        student.setCreateTime(new Date());

//        String jsonStr2 = mapper.writeValueAsString(student);
//        System.out.println(jsonStr2);

       /* 结果：
        {
            "name" : "hyl",
                "age" : 21,
                "sendTime" : "2020-07-23 13:14:36",
                "intList" : [ 1, 2, 3 ]
        }*/

        //js中将字符串转换为json对象
        //var data = "{\"name\":\"Hyl\", \"age\":20}";
        //var student = eval(data);
        //console.info(student.name);
        //console.info(student.age);


        /*结果：
        Hyl
        20*/
        return "";
    }

}
