package com.wk.utils;

import com.wk.pojo.User;
import org.springframework.beans.BeanUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.util.ClassUtils;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ReflectionUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;

public class SpringUtilsTest {

    public static void main(String[] args) throws ClassNotFoundException, IOException {
        String str = "com";
//        Class.forName(str);
        boolean present = ClassUtils.isPresent(str, null);
        System.out.println("present = " + present);
        System.out.println("str = " + str);
//获取本类,以及父类的属性
        ReflectionUtils.doWithFields(User.class, new ReflectionUtils.FieldCallback() {
            @Override
            public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                System.out.println("field = " + field.getName());
            }
        });
//获取本类,以及父类的方法
        ReflectionUtils.doWithMethods(User.class, new ReflectionUtils.MethodCallback() {
            @Override
            public void doWith(Method method) throws IllegalArgumentException, IllegalAccessException {
                System.out.println(method.getDeclaringClass().getName() + "=method.getName() = " + method.getName());
            }
        });
        Method[] allDeclaredMethods = ReflectionUtils.getAllDeclaredMethods(User.class);
//        ReflectionUtils.getField()
        User user = new User();
        user.setAge(1);
        ReflectionUtils.doWithFields(User.class, field -> {
                    field.setAccessible(true);
                    Object field1 = ReflectionUtils.getField(field, user);
                    System.out.println("field1 = " + field1);
                }
        );

        User user1 = new User();
        BeanUtils.copyProperties(user,user1);
        System.out.println("user1 = " + user1);

        ClassPathResource copy=new ClassPathResource("test.txt");
        EncodedResource encodedResource=new EncodedResource(copy, StandardCharsets.UTF_8);

        InputStream inputStream = encodedResource.getInputStream();
        String str1="D:\\A_code\\mybatis-plus\\kuang_livenote-master\\【遇见狂神说】MyBatisPlus视频笔记\\代码\\mybatis_plus\\src\\main\\resources\\test1.txt";

        FileCopyUtils.copy(inputStream,new FileOutputStream(str1));//文件内容复制
      //  user.insert(user1);
        /*Class<?> aClass = GenericTypeResolver.resolveReturnType(User.class, BaseDao.class);
        System.out.println("aClass.getName() = " + aClass.getName());*/

    }
}
