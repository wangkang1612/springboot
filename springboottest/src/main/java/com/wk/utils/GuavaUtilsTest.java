package com.wk.utils;

import com.google.common.base.CaseFormat;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.*;
import com.google.common.primitives.Ints;

import java.util.*;

public class GuavaUtilsTest {
    public static void main(String[] args) {
        //Joiner是连接器，Splitter是分割器，通常我们会把它们定义为static final
        //Joiner/Splitter
        //useForNull(String)对于Splitter  trimResults()/omitEmptyStrings()
        Joiner joiner = Joiner.on(",");//数组转字符串
        List list = new ArrayList();
        list.add("a");
        list.add("b");
        list.add("c");
        String join = joiner.join(list);

        System.out.println("join = " + join);//a,b,c
        Splitter splitter = Splitter.on(",");//字符串转数组
        List<String> strings = splitter.splitToList(join);
        System.out.println("strings = " + strings);//[a, b, c]


        //驼峰互转  str_name   strName
        String str = "str_name";
        String to = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, str);
        System.out.println("strName = " + to);//strName
        String strName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, "strName");
        System.out.println("str_name = " + strName);//str_name

        //lists,sets,maps
        ArrayList<String> strings1 = Lists.newArrayList("a", "b", "c");//[a, b]
        System.out.println("strings1 = " + strings1);
        List<List<String>> partition = com.google.common.collect.Lists.partition(strings, 2);
        System.out.println("partition = " + partition);//[[a, b], [c]]

        //ints longs
        List<Integer> integers = Ints.asList(1, 2, 3);
        System.out.println("integers = " + integers);//[1, 2, 3]
        //Multiset
        Multiset<String> multiset = HashMultiset.create();
        multiset.add("a");
        multiset.add("b");
        multiset.add("a");
        System.out.println("multiset1 = " + multiset.elementSet());//[a, b]
        System.out.println("multiset2 = " + multiset.entrySet());//[a x 2, b]
        Set<Multiset.Entry<String>> entries = multiset.entrySet();
        for (Multiset.Entry<String> entry : entries) {
            int count = entry.getCount();
            System.out.println("count = " + count);//2
        }
//multimap
        Multimap<String,String> multimap = HashMultimap.create();
        multimap.put("a","1");
        multimap.put("a","2");
        multimap.put("a","3");
        Collection<String> a = multimap.get("a");
        System.out.println("a = " + a);//[1, 2, 3]

        boolean a1 = multimap.containsEntry("a", "1");//true
        Map<String, Collection<String>> stringCollectionMap = multimap.asMap();
        System.out.println("stringCollectionMap = " + stringCollectionMap); //{a=[1, 2, 3]}
//immutableList  不要别人修改,放缓存,分布式  immutableMap
        ImmutableList<Object> immutableList = ImmutableList.builder().add("a").build();
//        immutableList.add("b");//不允许加

//Preconditions
        String str1 = Preconditions.checkNotNull(str, "不允许为null");
        str1=null;
        Preconditions.checkArgument(str1 ==null,"error");


    }
    }

