package com.wk.annotation;

import java.lang.annotation.*;

/**
 * ClassName Crmlog
 * AOP日志记录 自定义注解类
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface syslog {
    /**
     * 日志描述
     */
    String description()  default "";
}





