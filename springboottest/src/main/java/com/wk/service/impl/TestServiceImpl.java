package com.wk.service.impl;

import com.wk.pojo.CrmLogMessage;
import com.wk.service.TestService;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {

    @Override
    public String getLog(String input) {
        System.out.println("ouput = " + "请求数据库");
        return "ouput";
    }

    @Override
    public void insert(CrmLogMessage log) {
        System.out.println("insert = " + "插入数据库");
    }

    @Override
    public CrmLogMessage getOrderId(String orderId) {
        System.out.println("getOrderId = " + "查询幂等是否存在");
        return null;
    }
}
