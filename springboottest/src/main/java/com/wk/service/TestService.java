package com.wk.service;

import com.wk.pojo.CrmLogMessage;

public interface TestService {
    String getLog(String input);

    void insert(CrmLogMessage log);

    CrmLogMessage getOrderId(String orderId);
}
