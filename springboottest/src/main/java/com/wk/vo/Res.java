package com.wk.vo;

import com.sun.net.httpserver.Authenticator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static jdk.nashorn.tools.Shell.SUCCESS;

@Data
//序列化
@NoArgsConstructor
@AllArgsConstructor
public class Res<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;
    private String msg;
    private T data;
    private Boolean isSuccess;

    private final Map<String, Object> map = new HashMap<String, Object>();

    public void Res() {
        this.code = "0";
        this.msg = "success";
        this.isSuccess = true;
        this.data = null;
    }
    public void Res(String code,String msg,T data,Boolean isSuccess) {
        this.code = code;
        this.msg = msg;
        this.isSuccess = isSuccess;
        this.data = data;
    }

    public static <T> Res<T> suss(){
        return new Res<T>("0",null,null,true);
    }
    //成功返回
    public static Res success(String msg){
        return  new Res("0",msg,null,true);
    }
    //成功返回
    public static Res success(String msg,Object obj){
        return  new Res("0",msg,obj,true);
    }
    //失败返回
    public  static  Res  error(String msg){
        return  new Res("-1",msg,null,false);
    }
    //失败返回
    public  static  Res error(String msg,Object obj){
        return  new Res("-1",msg,obj,false);
    }

}
