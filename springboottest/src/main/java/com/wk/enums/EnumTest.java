package com.wk.enums;

public enum EnumTest {
    ONE("aa", "test1", 33),
    TWO("bb", "test2", 66),
    Three("cc", "test3", 99);
    private String a;
    private String b;
    private int c;

    EnumTest(String a, String b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
}
