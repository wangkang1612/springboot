package com.wk.controller;

import com.wk.annotation.syslog;
import com.wk.pojo.CrmLogMessage;
import com.wk.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
@Slf4j
@RestController
public class TestController {
    @Autowired
    TestService testService;
    //@syslogTest(description = "log123")
    private String name;

    @GetMapping("/hello")
    public String hello(){
        for (int i = 0; i < 100; i++) {
            System.out.println("args = " + i);
            log.info("args="+i);
        }
        return "hello";
    }

    @GetMapping("/a")
    public String a(){
        return "hello";
    }
    @syslog(description = "拦截接口")
    @GetMapping("/b")
    public String b(String param,HttpServletRequest request){
        System.out.println(" b= 1");
        String t=testService.getLog("input");
        System.out.println(" c= 1");
        return "hello";
    }

    @GetMapping("/addOrder")
    public String addorder(String orderId,HttpServletRequest request){
        System.out.println(" addorder  start");
        CrmLogMessage log = new CrmLogMessage();
        testService.insert(log);
        System.out.println(" addorder  end");
        return "ok";
    }


}