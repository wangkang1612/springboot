package com.wk;

import cn.hutool.core.text.csv.*;
import com.wk.utils.csv.DataUtil;
import com.wk.utils.csv.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.*;
import java.util.stream.Stream;
@Slf4j
public class demo {

    static Supplier<String> s = () -> "我是Supplier型接口";
    static BooleanSupplier bs = () -> true;
    static IntSupplier is = () -> 100;
    static LongSupplier ls = () -> 100L;
    static DoubleSupplier ds = () -> 100.00;

//blog.csdn.net/weixin_45840947/article/details/122656557
    public static void main(String[] args) {
        System.out.println("args = " + "00");
        //https://www.cnblogs.com/zhongjunbo555/p/11383159.html
        String str="test";
        String str1="input";
        String format = String.format("自定义输出{%s};内容【%s】", str, str1);
        System.out.println("format = " + format);
        String s = demo.s.get();
        System.out.println("s = " + s);
        List<String> strings = Arrays.asList("1", "1", "3");
        long count = strings.stream().filter(a -> a.contains("1")).count();
//        long count = strings.stream().count();
        System.out.println("count = " + count);
    }
////https://blog.csdn.net/yjltx1234csdn/article/details/121235803
    /**
     * 简单的 csv文件写入
     * @date 2021/11/8 13:51
     * @author zk_yjl
     * @param
     * @return void
     */
    @Test
    public void simpleWriteTest() throws Exception{
        //1. 大批量的业务处理，最后获取组装相应的数据信息。
        List<User> userList= DataUtil.createDataList("两个蝴蝶飞");
        //2. 存储的文件地址
        String filePath="D:\\csv\\simple.csv";
        //获取 CsvWriter
        CsvWriter csvWriter = CsvUtil.getWriter(filePath, Charset.forName("UTF-8"));
        // 写入注释
        csvWriter.writeComment("一个简单的csv文件");
        //写入新的一行
        csvWriter.writeLine();
        //写入内容
        csvWriter.writeBeans(userList);
        //关闭内容
        csvWriter.close();
        log.info("写入文件成功");
    }
    /**
     * 写入CsvData数据
     * @date 2021/11/8 14:06
     * @author zk_yjl
     * @param
     * @return void
     */
    @Test
    public void csvDataTest() throws Exception{
        //1. 大批量的业务处理，最后获取组装相应的数据信息。
        List<User> userList= DataUtil.createDataList("两个蝴蝶飞");
        //2. 存储的文件路径
        String filePath="D:\\csv\\data.csv";
        //3. 获取CsvWriter
        CsvWriter csvWriter = CsvUtil.getWriter(filePath, Charset.forName("UTF-8"));
        //4. 写入注入
        csvWriter.writeComment("写入CsvData文件");
        //写入标题头
        List<String> header=Arrays.asList("编号","姓名","性别","年龄","描述");

        List<CsvRow> csvRowList=new ArrayList<>();

        int i=0;
        //写入行
        for(User user:userList){
            //构建 headerMap
            Map<String, Integer> headerMap=new HashMap<>();
            headerMap.put("编号",0);
            headerMap.put("姓名",1);
            headerMap.put("性别",2);
            headerMap.put("年龄",3);
            headerMap.put("描述",4);

            //放置该行的值
            List<String> fieldList=new ArrayList<>();
            fieldList.add(user.getId()+"");
            fieldList.add(user.getName()+"");
            fieldList.add(user.getSex()+"");
            fieldList.add(user.getAge()+"");
            fieldList.add(user.getDescription()+"");
            //构建 CsvRow 对象
            CsvRow csvRow=new CsvRow(i,headerMap,fieldList);
            csvRowList.add(csvRow);
            i++;
        }
        CsvData csvData=new CsvData(header,csvRowList);
        csvWriter.write(csvData);
        //关闭
        csvWriter.close();
        log.info(">>写入CsvData 文件成功");
    }


    @Test
    public void simpleWriteTest1() throws Exception{
        //1. 大批量的业务处理，最后获取组装相应的数据信息。
        List<Map<String,String>> userList= DataUtil.createDataMapList();
        //2. 存储的文件地址
        String filePath="D:\\csv\\simple.csv";
        //获取 CsvWriter
        CsvWriter csvWriter = CsvUtil.getWriter(filePath, Charset.forName("UTF-8"));
        // 写入注释
        csvWriter.writeComment("一个简单的csv文件");
        //写入新的一行
        csvWriter.writeLine();
        //写入内容
        csvWriter.write(userList);
        //关闭内容
        csvWriter.close();
        log.info("写入文件成功");
    }
    /**
     * 简单文件的读取
     * @date 2021/11/8 14:15
     * @author zk_yjl
     * @param
     * @return void
     */
    @Test
    public void simpleTest(){
        //1. 读取的文件路径
        String filePath="D:\\csv\\simple.csv";
        //构建 CsvReader 对象
        CsvReader csvReader = CsvUtil.getReader();
        //进行读取
        CsvData csvData = csvReader.read(new File(filePath), Charset.forName("UTF-8"));
        //进行处理
        //获取相应的内容
        int rowCount = csvData.getRowCount();
        log.info(">>>读取了{}行",rowCount);
        //获取行数据
        List<CsvRow> rows = csvData.getRows();
        for(CsvRow csvRow:rows){
            //打印出该行的内容信息
            List<String> rawList = csvRow.getRawList();
            log.info(">>获取内容信息:"+rawList);
        }
    }
    /**
     * 读取信息，读取成 CsvData 形式.
     * @date 2021/11/8 14:39
     * @author zk_yjl
     * @param
     * @return void
     */
    @Test
    public void config1Test(){
        //1. 读取的文件路径
        String filePath="D:\\csv\\simple.csv";
        //2. 进行配置
        CsvReadConfig csvReadConfig=new CsvReadConfig();
        csvReadConfig.setSkipEmptyRows(true);
        //包括标题行
        csvReadConfig.setContainsHeader(true);
        //进行了读取，设置
        //构建 CsvReader 对象
        CsvReader csvReader = CsvUtil.getReader(csvReadConfig);
        //进行读取
        CsvData csvData = csvReader.read(new File(filePath), Charset.forName("UTF-8"));
        //进行处理
        List<String> header = csvData.getHeader();
        log.info(">>>读取的文件标题头为:"+header.toString());
        //获取相应的内容
        int rowCount = csvData.getRowCount();
        log.info(">>>读取了{}行",rowCount);
        //获取行数据
        List<CsvRow> rows = csvData.getRows();
        for(CsvRow csvRow:rows){
            List<String> rawList = csvRow.getRawList();
            log.info(">>获取内容信息:"+rawList);
        }
    }
    /**
     * 读取信息，读取成 CsvData 形式.
     * @date 2021/11/8 14:39
     * @author zk_yjl
     * @param
     * @return void
     */
    @Test
    public void config1Test1(){
        //1. 读取的文件路径
        String filePath="D:\\csv\\simple.csv";
        //2. 进行配置
        CsvReadConfig csvReadConfig=new CsvReadConfig();
        csvReadConfig.setSkipEmptyRows(true);
        //包括标题行
        csvReadConfig.setContainsHeader(true);
        //进行了读取，设置
        //构建 CsvReader 对象
        CsvReader csvReader = CsvUtil.getReader(csvReadConfig);
        //进行读取
        CsvData csvData = csvReader.read(new File(filePath), Charset.forName("UTF-8"));
        //进行处理
        List<String> header = csvData.getHeader();
        log.info(">>>读取的文件标题头为:"+header.toString());
        //获取相应的内容
        int rowCount = csvData.getRowCount();
        log.info(">>>读取了{}行",rowCount);
        //获取行数据
        List<CsvRow> rows = csvData.getRows();
        for(CsvRow csvRow:rows){
            List<String> rawList = csvRow.getRawList();
            log.info(">>获取内容信息:"+rawList);
        }
    }
    /**
     * 配置读取的信息
     * @date 2021/11/8 14:39
     * @author zk_yjl
     * @param
     * @return void
     */
    @Test
    public void config2Test(){
        //1. 读取的文件路径
        String filePath="D:\\csv\\simple.csv";
        //2. 进行配置
        CsvReadConfig csvReadConfig=new CsvReadConfig();
        csvReadConfig.setSkipEmptyRows(true);
        csvReadConfig.setContainsHeader(true);
        //设置最开始读取的行号, 注意，这一行，会被当成标题行的.
        csvReadConfig.setBeginLineNo(0);
        //读取的行号
        csvReadConfig.setEndLineNo(6);
        //进行了读取，设置
        //构建 CsvReader 对象
        CsvReader csvReader = CsvUtil.getReader(csvReadConfig);
        //进行读取
        CsvData csvData = csvReader.read(new File(filePath), Charset.forName("UTF-8"));
        //进行处理
        List<String> header = csvData.getHeader();
        log.info(">>>读取的文件标题头为:"+header.toString());
        //获取相应的内容
        int rowCount = csvData.getRowCount();
        log.info(">>>读取了{}行",rowCount);
        //获取行数据
        List<CsvRow> rows = csvData.getRows();
        for(CsvRow csvRow:rows){
           /* List<String> rawList = csvRow.getRawList();
            log.info(">>获取内容信息:"+rawList);*/
            log.info(">>>>");
            Map<String, String> fieldMap = csvRow.getFieldMap();
            for(Map.Entry<String,String> fieldEntry:fieldMap.entrySet()){
                log.info(">>>>>>>>:"+fieldEntry.getKey()+":"+fieldEntry.getValue());
            }
        }
    }

    /**
     * 读取信息，放置到bean 里面
     * @date 2021/11/8 14:39
     * @author zk_yjl
     * @param
     * @return void
     */
    @Test
    public void headerTest() throws Exception{
        //1. 读取的文件路径
        String filePath="D:\\csv\\header.csv";
        //进行了读取，设置
        //2. 进行配置
        CsvReadConfig csvReadConfig=new CsvReadConfig();
        csvReadConfig.setSkipEmptyRows(true);
        csvReadConfig.setContainsHeader(true);
        //构建 CsvReader 对象
        CsvReader csvReader = CsvUtil.getReader(csvReadConfig);
        //直接读取，封装成 Bean
        List<User> userList = csvReader.read(new FileReader(new File(filePath)), User.class);
        for(User user:userList){
            log.info(">>"+user);
        }
    }

}
