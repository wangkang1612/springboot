#!/bin/bash
allpath="/app/lulu-1.0.jar"
propath="/app/config/application.yml"
logpath="/applog/lulu-1.0-log-20230211.log"
#依赖jar 放到/app/lib后面
jarpath="/app/lib"

#1:不同环境配置  配置文件与项目分离

#linux 启动jar应用
nohub /app/jdk1.8.0_271/bin/java -jar ${allpath} --spring.config.location=${propath} -Xms2048m -Xmx1024m -Dfile.encoding=UTF-8 >>${logpath} &

#linux 启动jar应用 (应用程序与依赖jar分离)
nohub /app/jdk1.8.0_271/bin/java -Dloader.path=${jarpath} -jar ${allpath} --spring.config.location=${propath} -Xms2048m -Xmx1024m -Dfile.encoding=UTF-8 >>${logpath} &

#windows 启动jar    java -jar lulu-1.0.jar
D:\A_code\git\Spring\demo\test\springboottest\target>java -Dloader.path=./lib -jar lulu-1.0.jar

#2:验证jar是否正常启动，，并连接数据库
# 登堡垒机--账户密码--打开上传工具windowScp(上传jar到指定目录 eg:/app/home)--上传配置文件到对应的目录application.yml--打开Crt或者putty 进入大数据环境（已有联通数据库权限）--
# source /data/dcos/client8023/bigdata_env    (已安装运行的JDK环境，大数据客户端)
# java -jar ~/lulu-1.0.jar --spring.config.location=${propath}
#验证  大数据环境机器的ip:端口/lulu/hello     返回查询数据库是否成功   或者日志  tail -f lulu-1.0.log