package cn.itcast.demo;

import cn.itcast.demo.pojo.Heima62;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class SolrIndexSuperQuery {



    @Test
    public void testSortQuery() throws SolrServerException {
        SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/core2");


        //solr的查询条件
        SolrQuery solrQuery = new SolrQuery("title:手机");

        //给查询条件设置排序策略
        solrQuery.setSort("price", SolrQuery.ORDER.asc);

        //发起web请求查询solr中对应的数据，并获取响应
        QueryResponse queryResponse = solrServer.query(solrQuery);


        //把查询的返回结果解析到JavaBean中
        List<Heima62> heima62s = queryResponse.getBeans(Heima62.class);


        heima62s.forEach(heima62 -> {
            String id = heima62.getId();

            String title = heima62.getTitle();

            Long price = heima62.getPrice();

            System.out.println("id:"+id+" title:"+title+" price:"+price);
        });

    }


    @Test
    public void testPageQuery() throws SolrServerException {

        int currentPage = 20;
        final int  PAGE_SIZE = 5;
        int start = (currentPage-1)*PAGE_SIZE;



        SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/core2");


        //solr的查询条件
        SolrQuery solrQuery = new SolrQuery("title:手机");

        solrQuery.setStart(start);
        solrQuery.setRows(PAGE_SIZE);

        //发起web请求查询solr中对应的数据，并获取响应
        QueryResponse queryResponse = solrServer.query(solrQuery);


        //把查询的返回结果解析到JavaBean中
        List<Heima62> heima62s = queryResponse.getBeans(Heima62.class);


        heima62s.forEach(heima62 -> {
            String id = heima62.getId();

            String title = heima62.getTitle();

            Long price = heima62.getPrice();

            System.out.println("id:"+id+" title:"+title+" price:"+price);
        });

    }



    @Test
    public void testHighLightQuery() throws SolrServerException {

        SolrServer solrServer = new HttpSolrServer("http://localhost:8080/solr/core2");


        //solr的查询条件
        SolrQuery solrQuery = new SolrQuery("*:*");

        //添加高亮查询的条件信息
        solrQuery.setHighlightSimplePre("<em>");
        solrQuery.setHighlightSimplePost("</em>");
        solrQuery.addHighlightField("title");




        //发起web请求查询solr中对应的数据，并获取响应
        QueryResponse queryResponse = solrServer.query(solrQuery);


        //把查询的返回结果解析到JavaBean中
        List<Heima62> heima62s = queryResponse.getBeans(Heima62.class);


        //从响应的结果中取出高亮的结果,双层map，第一次map的key为文档的id，第二层map的key为高亮字段的名称，第二层map的value为高亮的结果
        Map<String, Map<String, List<String>>> highlighting = queryResponse.getHighlighting();


        heima62s.forEach(heima62 -> {
            String id = heima62.getId();

            String highLightTitle = highlighting.get(id).get("title").get(0);

            String title = heima62.getTitle();

            Long price = heima62.getPrice();

            System.out.println("id:"+id+" title:"+highLightTitle+" price:"+price);
        });

    }




}
