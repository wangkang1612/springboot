package cn.itcast.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.sql.DataSource;

@Controller
public class HelloController {


    @Autowired
    private DataSource dataSource;

    @GetMapping("hello")
    @ResponseBody
    public String sayHello(){
        return "hello heima62 everyone"+dataSource;
    }
}
