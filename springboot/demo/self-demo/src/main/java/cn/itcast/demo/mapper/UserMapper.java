package cn.itcast.demo.mapper;

import cn.itcast.demo.pojo.User;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;


public interface UserMapper extends Mapper<User> {

    User queryUserById(@Param("id") Long id);
}
