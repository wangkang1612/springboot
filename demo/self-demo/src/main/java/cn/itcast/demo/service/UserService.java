package cn.itcast.demo.service;

import cn.itcast.demo.mapper.UserMapper;
import cn.itcast.demo.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public User queryUserById(Long id) {
        return this.userMapper.selectByPrimaryKey(id);
    }


    @Transactional//1个
    public void deleteById(Long id){
        this.userMapper.deleteByPrimaryKey(id);
    }
}
